<?php namespace Routemedia\Memoar;

use Illuminate\Support\Str;

/**
* Observer base class
*/
class Observer {

	protected $model;

	protected $summaryGroup;

	protected function setModel(HistorableInterface $model)
	{
		$this->model = $model;
	}

	public function created(HistorableInterface $model) {
		$this->setModel($model);
		$this->saveHistory('created');
	}

	public function updated(HistorableInterface $model) {
		$this->setModel($model);
		$diff = $this->getDiff();

		// when listening to laravel restored event,
		// there are triggering updated and restored event
		// use this hack to solve restored event problem
		if (! array_key_exists($model::DELETED_AT, $diff)) {
			$this->saveHistory('updated');
		}
		else {
			$this->saveHistory('restored');
		}
	}

	public function deleted(HistorableInterface $model) {
		$this->setModel($model);
		$this->saveHistory('deleted');
	}

	protected function saveHistory($event) {
		$diff = $this->getDiff();
		$customFields = $this->getCustomFields();
		$data = array(
			'diff' => $diff,
			'summary' => $this->getSummary($event)
		);
		$history = new History($data);

		if (! empty($this->getCustomFields())) {
			foreach ($customFields as $field => $callback) {
				if (is_int($field)) {
					$field = $callback;
					$callback = Str::camel($field);
				}

				if ($callback instanceof \Closure) {
					$value = $callback($diff, $event);
				}
				else {
					if (method_exists($this, $callback)) {
						$value = call_user_func_array(array($this, $callback), array($diff, $event));
					}
					else {
						throw new \CustomFieldMethodNotFoundException("Error Processing Request", 1);
					}
				}

				$history->{$field} = $value;
			}
		}

		$this->model->getHistory()->save($history);
	}

	protected function getCustomFields() {
		return array();
	}

	protected function getSummary($event)
	{
		return $this->getSummaryGroup($event);
	}

	protected function getDiff() {
		$original = $this->stringify($this->model->getOriginal());
		$attributes = $this->stringify($this->model->getAttributes());
		$diff = array_diff($original, $attributes);

		if (empty($diff)) {
			$diff = $this->model->getOriginal();
		}

		return $diff;
	}

	protected function stringify(array $array) {
		foreach ($array as $key => $value) {
			if ($value instanceof \DateTime) {
				$array[$key] = $value->format('Y-m-d H:i:s');
			}
		}

		return $array;
	}

	protected function getSummaryGroup($event = null) {
		$group = (! empty($this->summaryGroup))
			? $this->summaryGroup
			: str_replace('\\', '', snake_case(get_class($this->model)));

		if ($event) {
			return $group . '.' . $event;
		}

		return $group;
	}

}

class CustomFieldMethodNotFoundException extends \Exception {}