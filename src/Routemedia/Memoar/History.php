<?php namespace Routemedia\Memoar;

use Illuminate\Database\Eloquent\Model;

/**
* History
*/
class History extends Model
{
    protected $table = 'rtm_memoar';

    protected $fillable = array('summary', 'diff');

    public $timestamps = false;

    public function historable()
    {
        return $this->morphTo();
    }
    
    public function setDiffAttribute($value)
    {
        $this->attributes['diff'] = serialize($value);
    }

    public function getDiffAttribute()
    {
        return unserialize($this->attributes['diff']);
    }

    public function getDates()
    {
        return array('added_on');
    }

    public static function boot() {
        parent::boot();

        History::creating(function($model) {
            $model->added_on = new \DateTime;
        });
    }
}