<?php namespace Routemedia\Memoar;

/**
* HistorableInterface
*/
interface HistorableInterface
{
    public function getHistory();
}