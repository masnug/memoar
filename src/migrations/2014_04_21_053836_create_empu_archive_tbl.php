<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpuArchiveTbl extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::dropIfExists('rtm_memoar');
		
		Schema::create('rtm_memoar', function(Blueprint $table) {
			$table->engine = 'Archive';

			$table->bigIncrements('id');
			$table->string('summary');
			$table->text('diff');
			$table->bigInteger('historable_id')->unsigned();
			$table->string('historable_type');
			$table->timestamp('added_on');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rtm_memoar');
	}

}
