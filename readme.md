Memoar
======

This is time machine for your database, bring you data tracking capabilities. [https://packagist.org/packages/routemedia/memoar]

Usage
-----

Using Memoar is simple:

First, run package migration

    ./artisan migrate --package="routemedia/memoar"

Next, implement the interface Historable and define `histories` and `getHistory` methods to any model you wish to perform data tracking.

<!-- lang-php -->

    use Routemedia\Memoar\HistorableInterface;

    class Company extends Eloquent implements HistorableInterface {

        public function histories() // polymorphic relation
        {
            return $this->morphMany('Routemedia\Memoar\History', 'historable');
        }

        public function getHistory()
        {
            // return Routemedia\Memoar\History polymorphic relation
            return $this->histories();
        }
        // ...

Register an observer instance using the observe method:

<!-- lang-php -->

    Company::observe(new Routemedia\Memoar\Observer);

And do anything to your model, Memoar will track any data change.

Custom Fields
-------------

By default, Memoar write summary and diff of data change to archive table.
You may add more fields, for example `director`. To make this, create your
custom observer extends `Routemedia\Memoar\Observer` and override the
`getCustomFields` method.

<!-- lang-php -->

    use Routemedia\Memoar\Observer;

    /**
    * CustomObserver
    */
    class CustomObserver extends Observer
    {
        public function getCustomFields()
        {
            return array(
                'director' => function($diff, $event) {
                    return \Auth::user()->username;

                },
                'custom_field', // will call setCustomField method
                'other_field' => 'dummyMethod'
            );
        }

        public function setCustomField($diff, $event) { /* custom_field */ }

        public function dummyMethod($diff, $event) { /* other_field */ }
    }